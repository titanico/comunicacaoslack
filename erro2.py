# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'erro.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Error2(object):
    def setupUi(self, Error2):
        Error2.setObjectName("Error2")
        Error2.setFixedSize(245, 75)
        self.falha = QtWidgets.QLabel(Error2)
        self.falha.setGeometry(QtCore.QRect(20, 20, 210, 31))
        self.falha.setObjectName("falha")

        self.retranslateUi(Error2)
        QtCore.QMetaObject.connectSlotsByName(Error2)

    def retranslateUi(self, Error2):
        _translate = QtCore.QCoreApplication.translate
        Error2.setWindowTitle(_translate("Error2", "ERRO"))
        self.falha.setText(_translate("Error2", "         Falha ao enviar a mensagem. \n Todos os campos devem ser preenchidos. "))




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Error2 = QtWidgets.QDialog()
    ui = Ui_Error2()
    ui.setupUi(Error2)
    Error2.show()
    sys.exit(app.exec_())
