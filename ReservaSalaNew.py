# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_ui_comm.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import json, requests
from SlackAPI import SlackAPI
from datetime import datetime
from Sucesso import *
import socket
from erro import *
from erro2 import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setFixedSize(400, 576)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.lineEditNomePessoa = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditNomePessoa.setObjectName("lineEditNomePessoa")
        self.horizontalLayout.addWidget(self.lineEditNomePessoa)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.lineEditDiretoria = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditDiretoria.setObjectName("lineEditDiretoria")
        self.horizontalLayout_2.addWidget(self.lineEditDiretoria)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_5.addWidget(self.label_7)
        self.lineEditTelefone = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditTelefone.setObjectName("lineEditTelefone")
        self.horizontalLayout_5.addWidget(self.lineEditTelefone)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.verticalLayout_5.addLayout(self.verticalLayout)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_5.addWidget(self.line_3)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.verticalLayout_2.addWidget(self.label_4)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_2.addWidget(self.line_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_3.addWidget(self.label_5)
        self.lineEditNomeEvento = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditNomeEvento.setObjectName("lineEditNomeEvento")
        self.horizontalLayout_3.addWidget(self.lineEditNomeEvento)
        self.verticalLayout_2.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_4.addWidget(self.label_6)
        self.dateEditEvento = QtWidgets.QDateEdit(self.centralwidget)
        self.dateEditEvento.setDate(QtCore.QDate(2019, 1, 1))
        self.dateEditEvento.setObjectName("dateEditEvento")
        self.horizontalLayout_4.addWidget(self.dateEditEvento)
        self.verticalLayout_2.addLayout(self.horizontalLayout_4)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_6.addWidget(self.label_10)
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.horizontalLayout_6.addWidget(self.comboBox)
        self.verticalLayout_3.addLayout(self.horizontalLayout_6)
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setObjectName("label_8")
        self.verticalLayout_3.addWidget(self.label_8)
        self.textEditReq = QtWidgets.QTextEdit(self.centralwidget)
        self.textEditReq.setObjectName("textEditReq")
        self.verticalLayout_3.addWidget(self.textEditReq)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setObjectName("label_9")
        self.verticalLayout_4.addWidget(self.label_9)
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout_4.addWidget(self.textEdit)
        self.verticalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout_5.addLayout(self.verticalLayout_2)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.pushButtonLimpar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonLimpar.setObjectName("pushButtonLimpar")
        self.gridLayout.addWidget(self.pushButtonLimpar, 1, 1, 1, 1)
        self.pushButtonVoltar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonVoltar.setObjectName("pushButtonVoltar")
        self.gridLayout.addWidget(self.pushButtonVoltar, 1, 2, 1, 1)
        self.pushButtonEnviar = QtWidgets.QPushButton(self.centralwidget)
        self.pushButtonEnviar.setObjectName("pushButtonEnviar")
        self.gridLayout.addWidget(self.pushButtonEnviar, 1, 0, 1, 1)
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.gridLayout.addWidget(self.line_4, 0, 0, 1, 1)
        self.verticalLayout_5.addLayout(self.gridLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 400, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.pushButtonLimpar.clicked.connect(self.textEditReq.clear)
        self.pushButtonLimpar.clicked.connect(self.lineEditNomePessoa.clear)
        self.pushButtonLimpar.clicked.connect(self.lineEditNomeEvento.clear)
        self.pushButtonLimpar.clicked.connect(self.lineEditTelefone.clear)
        self.pushButtonLimpar.clicked.connect(self.lineEditDiretoria.clear)
        self.pushButtonLimpar.clicked.connect(self.dateEditEvento.clear)
        self.pushButtonLimpar.clicked.connect(self.comboBox.clear)
        self.pushButtonLimpar.clicked.connect(self.textEditReq.clear)
        self.pushButtonVoltar.clicked.connect(QtCore.QCoreApplication.instance().quit)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Reserva de Salas"))
        self.label.setText(_translate("MainWindow", "Pessoa:"))
        self.label_2.setText(_translate("MainWindow", "Nome:"))
        self.label_3.setText(_translate("MainWindow", "Diretoria:"))
        self.label_7.setText(_translate("MainWindow", "Telefone: "))
        self.label_4.setText(_translate("MainWindow", "Evento:"))
        self.label_5.setText(_translate("MainWindow", "Nome:"))
        self.label_6.setText(_translate("MainWindow", "Data: "))
        self.label_10.setText(_translate("MainWindow", "Sala: "))
        self.comboBox.setItemText(0, _translate("MainWindow", "Sala de aula"))
        self.comboBox.setItemText(1, _translate("MainWindow", "Sala de reunião"))
        self.comboBox.setItemText(2, _translate("MainWindow", "Laboratório"))
        self.comboBox.setItemText(3, _translate("MainWindow", "Outro"))
        self.label_8.setText(_translate("MainWindow", "Requisitos:"))
        self.label_9.setText(_translate("MainWindow", "Observações:"))
        self.pushButtonLimpar.setText(_translate("MainWindow", "Limpar"))
        self.pushButtonVoltar.setText(_translate("MainWindow", "Voltar"))
        self.pushButtonEnviar.setText(_translate("MainWindow", "Enviar"))

        self.x = SlackAPI('https://hooks.slack.com/services/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')
        self.pushButtonEnviar.clicked.connect(lambda: self.SlackMsg())


    def SlackMsg(self):
     
        try:
            socket.create_connection(("titanci.slack.com",80))

            nome = self.lineEditNomePessoa.text()
            diretoria = self.lineEditDiretoria.text()
            telefone = self.lineEditTelefone.text()
            evento = self.lineEditNomeEvento.text()
            data = self.dateEditEvento.text()
            sala = self.comboBox.currentText()
            requisitos = self.textEditReq.toPlainText()
            obs = self.textEdit.toPlainText()


            if (nome != "" and diretoria != "" and evento != "" and data != "" and telefone != "" and sala != "" and requisitos != ""):

                self.window = QtWidgets.QFrame()
                self.suc = Ui_Frame()
                self.suc.setupUi(self.window)
                self.window.show()

                now = datetime.now()
                day = str(now.day)
                dia = int(day)
                month = str(now.month)
                mes = int(month)
                year = str(now.year)
                hour = str(now.hour)
                hora = int(hour)
                minu = str(now.minute)
                minn = int(minu)
                if (dia<10):
                    day = '0'+day
                if (mes<10):
                    month = '0'+month
                if (hora<10):
                    hour = '0'+hour
                if (minn<10):
                    minu = '0'+minu 
                

                message = {"text":'*'+'[SOLICITAÇÃO DE RESERVA DE SALA]'+'*'+'\n'+'*(Data da solicitação:* '+day+'/'+month+'/'+year+'   *Hora da solicitação:* '+hour+':'+minu+' * )*'+'\n'+'*Responsável:* '+nome+'\n'+'*Diretoria:* '+diretoria+'\n'+'*Telefone:* '+telefone+'\n'+'*Evento:* '+evento+'\n'+'*Dia:* '+data+'\n'+'*Sala:* '+sala+'\n'+'*Requisitos:* '+requisitos+'\n'+'*Observações:* '+obs}
                self.x.SendMsg(message)
            else:
                self.window = QtWidgets.QMainWindow()
                self.err2 = Ui_Error2()
                self.err2.setupUi(self.window)
                self.window.show()
        except OSError:
            pass
            self.window = QtWidgets.QMainWindow()
            self.err = Ui_Error()
            self.err.setupUi(self.window)
            self.window.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
