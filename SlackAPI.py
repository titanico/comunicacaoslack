import json, requests

class SlackAPI():
    def __init__(self,WEBHOOK):
        self.WEBHOOK = WEBHOOK

    def SendMsg(self,message):
        data = json.dumps(message)
        r = requests.post(
            url = self.WEBHOOK,
            data = data,
            headers = {'Content-Type': 'application/json'}
        )
        if str(r) != '<Response [200]>':
            return False
        return True
