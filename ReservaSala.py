﻿# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'REQ.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import json, requests
from SlackAPI import SlackAPI
from datetime import datetime
from Sucesso import *
import socket
from erro import *
from erro2 import *




class Ui_REQ(object):
    def setupUi(self, REQ):
        REQ.setObjectName("REQ")
        REQ.setFixedSize(435, 467)
        self.centralwidget = QtWidgets.QWidget(REQ)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 412, 336))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.verticalLayout.setContentsMargins(10, 0, 10, 1)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.nome = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.nome.setFont(font)
        self.nome.setObjectName("nome")
        self.horizontalLayout_2.addWidget(self.nome)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_2.addWidget(self.lineEdit_2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.diret = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.diret.setFont(font)
        self.diret.setObjectName("diret")
        self.horizontalLayout_3.addWidget(self.diret)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.horizontalLayout_3.addWidget(self.lineEdit_3)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.data = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.data.setFont(font)
        self.data.setObjectName("data")
        self.verticalLayout_4.addWidget(self.data)
        self.qtd = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.qtd.setFont(font)
        self.qtd.setObjectName("qtd")
        self.verticalLayout_4.addWidget(self.qtd)
        self.horizontalLayout_10.addLayout(self.verticalLayout_4)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.dateEdit = QtWidgets.QDateEdit(self.verticalLayoutWidget)
        self.dateEdit.setDate(QtCore.QDate(2019, 1, 2))
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setObjectName("dateEdit")
        self.verticalLayout_5.addWidget(self.dateEdit)
        self.lineEdit = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit.setObjectName("lineEdit")
        self.verticalLayout_5.addWidget(self.lineEdit)
        self.horizontalLayout_10.addLayout(self.verticalLayout_5)
        self.verticalLayout.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.horizontalLayout_5.addWidget(self.label)
        self.lineEdit_4 = QtWidgets.QLineEdit(self.verticalLayoutWidget)
        self.lineEdit_4.setObjectName("lineEdit_4")
        self.horizontalLayout_5.addWidget(self.lineEdit_4)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.hri = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.hri.setFont(font)
        self.hri.setObjectName("hri")
        self.verticalLayout_3.addWidget(self.hri)
        self.hrt = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.hrt.setFont(font)
        self.hrt.setObjectName("hrt")
        self.verticalLayout_3.addWidget(self.hrt)
        self.horizontalLayout_9.addLayout(self.verticalLayout_3)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.timeEdit = QtWidgets.QTimeEdit(self.verticalLayoutWidget)
        self.timeEdit.setDate(QtCore.QDate(2019, 1, 1))
        self.timeEdit.setObjectName("timeEdit")
        self.verticalLayout_2.addWidget(self.timeEdit)
        self.timeEdit_2 = QtWidgets.QTimeEdit(self.verticalLayoutWidget)
        self.timeEdit_2.setObjectName("timeEdit_2")
        self.verticalLayout_2.addWidget(self.timeEdit_2)
        self.horizontalLayout_9.addLayout(self.verticalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout_9)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.motivo = QtWidgets.QLabel(self.verticalLayoutWidget)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.motivo.setFont(font)
        self.motivo.setObjectName("motivo")
        self.horizontalLayout.addWidget(self.motivo)
        self.requisicao_text_motivo = QtWidgets.QTextEdit(self.verticalLayoutWidget)
        self.requisicao_text_motivo.setObjectName("textEdit1")
        self.horizontalLayout.addWidget(self.requisicao_text_motivo)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayoutWidget_4 = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget_4.setGeometry(QtCore.QRect(20, 400, 392, 29))
        self.horizontalLayoutWidget_4.setObjectName("horizontalLayoutWidget_4")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_4)
        self.horizontalLayout_4.setContentsMargins(20, 0, 20, 0)
        self.horizontalLayout_4.setSpacing(20)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.requisicao_botao_enviar = QtWidgets.QPushButton(self.horizontalLayoutWidget_4)
        self.requisicao_botao_enviar.setObjectName("btreq1")
        self.horizontalLayout_4.addWidget(self.requisicao_botao_enviar)
        self.requisicao_botao_limpar = QtWidgets.QPushButton(self.horizontalLayoutWidget_4)
        self.requisicao_botao_limpar.setObjectName("btreq_2")
        self.horizontalLayout_4.addWidget(self.requisicao_botao_limpar)
        self.requisicao_botao_logout = QtWidgets.QPushButton(self.horizontalLayoutWidget_4)
        self.requisicao_botao_logout.setObjectName("btreq_3")
        self.horizontalLayout_4.addWidget(self.requisicao_botao_logout)
        REQ.setCentralWidget(self.centralwidget)

        self.retranslateUi(REQ)
        self.requisicao_botao_limpar.clicked.connect(self.requisicao_text_motivo.clear)
        self.requisicao_botao_limpar.clicked.connect(self.timeEdit_2.clear)
        self.requisicao_botao_limpar.clicked.connect(self.timeEdit.clear)
        self.requisicao_botao_limpar.clicked.connect(self.lineEdit.clear)
        self.requisicao_botao_limpar.clicked.connect(self.dateEdit.clear)
        self.requisicao_botao_limpar.clicked.connect(self.lineEdit_3.clear)
        self.requisicao_botao_limpar.clicked.connect(self.lineEdit_2.clear)
        self.requisicao_botao_limpar.clicked.connect(self.lineEdit_4.clear)
        self.requisicao_botao_logout.clicked.connect(QtCore.QCoreApplication.instance().quit)
        QtCore.QMetaObject.connectSlotsByName(REQ)
    

    def retranslateUi(self, REQ):
        _translate = QtCore.QCoreApplication.translate
        REQ.setWindowTitle(_translate("REQ", "Reserva Sala"))
        self.nome.setText(_translate("REQ", "NOME:"))
        self.diret.setText(_translate("REQ", "DIRETORIA:"))
        self.data.setText(_translate("REQ", "DATA:"))
        self.qtd.setText(_translate("REQ", "QUANTIDADE DE PARTICIPANTES:"))
        self.dateEdit.setDisplayFormat(_translate("REQ", "dd/MM/yyyy"))
        self.label.setText(_translate("REQ", "SALA:"))
        self.hri.setText(_translate("REQ", "HORÁRIO DE INICIO:"))
        self.hrt.setText(_translate("REQ", "HORÁRIO DE TÉRMINO:"))
        self.timeEdit.setDisplayFormat(_translate("REQ", "HH:mm"))
        self.motivo.setText(_translate("REQ", "MOTIVO:"))
        self.requisicao_botao_enviar.setText(_translate("REQ", "ENVIAR"))
        self.requisicao_botao_limpar.setText(_translate("REQ", "LIMPAR"))
        self.requisicao_botao_logout.setText(_translate("REQ", "VOLTAR"))

        self.x = SlackAPI('https://hooks.slack.com/services/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')
        self.requisicao_botao_enviar.clicked.connect(lambda: self.SlackMsg())
        
        
    def SlackMsg(self):
     
        try:
            socket.create_connection(("titanci.slack.com",80))

            nome = self.lineEdit_2.text()
            diretoria = self.lineEdit_3.text()
            motivo = self.requisicao_text_motivo.toPlainText()
            hri = self.timeEdit.text()
            hrt = self.timeEdit_2.text()
            data = self.dateEdit.text()
            qtd = self.lineEdit.text()
            sala = self.lineEdit_4.text()

            if (nome != "" and diretoria != "" and motivo != "" and hri != "" and hrt != "" and data != "" and qtd != "" and sala != ""):

                self.window = QtWidgets.QFrame()
                self.suc = Ui_Frame()
                self.suc.setupUi(self.window)
                self.window.show()

                now = datetime.now()
                day = str(now.day)
                dia = int(day)
                month = str(now.month)
                mes = int(month)
                year = str(now.year)
                hour = str(now.hour)
                hora = int(hour)
                minu = str(now.minute)
                minn = int(minu)
                if (dia<10):
                    day = '0'+day
                if (mes<10):
                    month = '0'+month
                if (hora<10):
                    hour = '0'+hour
                if (minn<10):
                    minu = '0'+minu 
                

                message = {"text":'*'+'[SOLICITAÇÃO DE RESERVA DE SALA]'+'*'+'\n'+'*(Data da solicitação:* '+day+'/'+month+'/'+year+'   *Hora da solicitação:* '+hour+':'+minu+' * )*'+'\n'+'*Responsável:* '+nome+'\n'+'*Diretoria:* '+diretoria+'\n'+'*Sala:* '+sala+'\n'+'*Dia:* '+data+'\n'+'*Horário de Início:* '+hri+'\n'+'*Horário de Término:* '+hrt+'\n'+'*Quantidade de Participantes:* '+qtd+'\n'+'*Motivo:* '+motivo}
                self.x.SendMsg(message)
            else:
                self.window = QtWidgets.QMainWindow()
                self.err2 = Ui_Error2()
                self.err2.setupUi(self.window)
                self.window.show()
        except OSError:
            pass
            self.window = QtWidgets.QMainWindow()
            self.err = Ui_Error()
            self.err.setupUi(self.window)
            self.window.show()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    REQ = QtWidgets.QMainWindow()
    ui = Ui_REQ()
    ui.setupUi(REQ)
    REQ.show()
    sys.exit(app.exec_())