﻿# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Interface.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import json, requests
from SlackAPI import SlackAPI
from Sucesso import *
import socket
from erro import *
from erro2 import *

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setFixedSize(400, 300)
        Dialog.setAutoFillBackground(True)
        self.comunicado_botao_limpar = QtWidgets.QPushButton(Dialog)
        self.comunicado_botao_limpar.setGeometry(QtCore.QRect(230, 270, 75, 23))
        self.comunicado_botao_limpar.setObjectName("pushButton")
        self.comunicado_botao_enviar = QtWidgets.QPushButton(Dialog)
        self.comunicado_botao_enviar.setGeometry(QtCore.QRect(310, 270, 75, 23))
        self.comunicado_botao_enviar.setObjectName("pushButton2")
        self.textEdit = QtWidgets.QTextEdit(Dialog)
        self.textEdit.setGeometry(QtCore.QRect(60, 20, 321, 21))
        self.textEdit.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textEdit.setObjectName("textEdit")
        self.autor = QtWidgets.QLabel(Dialog)
        self.autor.setGeometry(QtCore.QRect(10, 10, 51, 41))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setKerning(True)
        self.autor.setFont(font)
        self.autor.setMouseTracking(False)
        self.autor.setAutoFillBackground(False)
        self.autor.setObjectName("autor")
        self.titulo = QtWidgets.QLabel(Dialog)
        self.titulo.setGeometry(QtCore.QRect(10, 60, 41, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.titulo.setFont(font)
        self.titulo.setObjectName("titulo")
        self.mensagem = QtWidgets.QLabel(Dialog)
        self.mensagem.setGeometry(QtCore.QRect(150, 120, 81, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.mensagem.setFont(font)
        self.mensagem.setObjectName("mensagem")
        self.textEdit_2 = QtWidgets.QTextEdit(Dialog)
        self.textEdit_2.setGeometry(QtCore.QRect(60, 70, 321, 21))
        self.textEdit_2.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.textEdit_2.setObjectName("textEdit_2")
        self.textEdit_3 = QtWidgets.QTextEdit(Dialog)
        self.textEdit_3.setGeometry(QtCore.QRect(10, 150, 371, 101))
        self.textEdit_3.setObjectName("textEdit_3")

        self.retranslateUi(Dialog)
        self.comunicado_botao_limpar.clicked.connect(self.textEdit_3.clear)
        self.comunicado_botao_limpar.clicked.connect(self.textEdit_2.clear)
        self.comunicado_botao_limpar.clicked.connect(self.textEdit.clear)
        self.comunicado_botao_enviar.clicked.connect(self.textEdit_3.clear)
        self.comunicado_botao_enviar.clicked.connect(self.textEdit_2.clear)
        self.comunicado_botao_enviar.clicked.connect(self.textEdit.clear)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Titânico"))
        self.comunicado_botao_limpar.setText(_translate("Dialog", "Limpar"))
        self.comunicado_botao_enviar.setText(_translate("Dialog", "Enviar"))
        self.autor.setText(_translate("Dialog", "Autor:"))
        self.titulo.setText(_translate("Dialog", "Título:"))
        self.mensagem.setText(_translate("Dialog", "Mensagem:"))

        self.x = SlackAPI('https://hooks.slack.com/services/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')

        self.comunicado_botao_enviar.clicked.connect(lambda: self.slackMsg())
        
    def slackMsg(self):
        try:
            socket.create_connection(("titanci.slack.com",80))
            
            autor = self.textEdit.toPlainText()
            titulo = self.textEdit_2.toPlainText()
            texto = self.textEdit_3.toPlainText()

            if (autor != "" and titulo != "" and texto != ""):

                self.window = QtWidgets.QFrame()
                self.suc = Ui_Frame()
                self.suc.setupUi(self.window)
                self.window.show()

                message = {"text":'*'+titulo+'*'+'\n'+texto+'\n'+'`autor:`'+autor}
                self.x.SendMsg(message)
            else:
                self.window = QtWidgets.QMainWindow()
                self.err2 = Ui_Error2()
                self.err2.setupUi(self.window)
                self.window.show()
        except OSError:
            pass
            self.window = QtWidgets.QMainWindow()
            self.err = Ui_Error()
            self.err.setupUi(self.window)
            self.window.show()

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv).instance()
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

