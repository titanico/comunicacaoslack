﻿# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MENU.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from ReservaSalaNew import *
from Comunicados import Ui_Dialog
from SlackAPI import SlackAPI
import json, requests
import sys
from Sucesso import *

class Ui_MENU(object):
    def openreq(self):
        self.window = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.window)
        self.window.show()
        
    
    def openLI(self):
        self.windows = QtWidgets.QMainWindow()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self.windows)
        self.windows.show()
    
    def setupUi(self, MENU):
        MENU.setObjectName("MENU")
        MENU.setFixedSize(348, 142)
        self.centralwidget = QtWidgets.QWidget(MENU)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(40, 30, 263, 80))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label, 0, QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(40)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.menu_botao_LI = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.menu_botao_LI.setObjectName("menu1")
        self.horizontalLayout.addWidget(self.menu_botao_LI)
        self.menu_botao_requisicao = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.menu_botao_requisicao.setObjectName("menu21")
        self.horizontalLayout.addWidget(self.menu_botao_requisicao)
        self.verticalLayout.addLayout(self.horizontalLayout)
        MENU.setCentralWidget(self.centralwidget)

        self.retranslateUi(MENU)
        self.menu_botao_LI.clicked.connect(self.openLI)
        self.menu_botao_requisicao.clicked.connect(self.openreq)

        QtCore.QMetaObject.connectSlotsByName(MENU)

    def retranslateUi(self, MENU):
        _translate = QtCore.QCoreApplication.translate
        MENU.setWindowTitle(_translate("MENU", "COMUNICAÇÃO SLACK"))
        self.label.setText(_translate("MENU", "O QUE DESEJA?"))
        self.menu_botao_LI.setText(_translate("MENU", "COMUNICADO\n"
"SLACK"))
        self.menu_botao_requisicao.setText(_translate("MENU", "REQUISITAR \n"
"UMA SALA"))


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MENU = QtWidgets.QMainWindow()
    ui = Ui_MENU()
    ui.setupUi(MENU)
    MENU.show()
    sys.exit(app.exec_())

