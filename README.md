Orientações:
--------------------

A reserva de salas pela TITAN se tornou uma necessidade recorrente devido as constantes capacitações, encontros e apresentações da mesma que ocorrem no espaço físico da UFBa e, mais especificamente e de maneira mais recorrente, nos laboratórios da EPUFBa. A reserva de sala, onde incluem-se **a) elencagem das informações necessárias para solicitação**, **b) confecção do e-mail**, **c) envio do e-mail**, **d) aguardo da resposta** e **e) divulgação da resposta**, pode ser considerada um processo interno da TITAN cuja responsabilidade é do setor Administrativo-Financeiro(DAF).

Dito isso, a partir de uma aplicação _desktop_ já existente da TITAN, desenvolvida pelos grupos 1 e 2 dos _trainees_, o **ComunicacaoSlack**, cujo código fonte se encontra disponível neste repositório, vocês deverão desenvolver a seguinte melhoria: além de enviar comunicados gerais pelo Slack, a aplicação também deverá permitir a solicitação reservas de sala ao DAF, postando tais solicitações em um canal privado do Slack destinado a este fim.

Para auxiliá-los nesta tarefa, disponibilizamos:

1. A [**Wiki**](https://bitbucket.org/titanico/comunicacaoslack/wiki/Home) onde encontram-se os manuais referentes a aplicação atual do **ComunicacaoSlack** e um breve manual sobre o PyQt5, a instalação do Anaconda e como utilizar a ferramenta de construção de interfaces Qt Designer;

2. O time de _trainees_ atual do DAF para reuniões de requisitos e teste da aplicação desenvolvida.


Requisitos:
----------------------

### Funcionais
1. Como será mantida a aplicação de envio de comunicado em conjunto com a melhoria, será necessária uma tela de seleção visível ao usuário para que o mesmo escolha qual serviço utilizará: o envio de comunicado ou a solicitação de reserva de sala;
2. Deverá ser desenvolvido um formulário digital: uma tela com campos preenchíveis e um botão de envio e outro de cancelar;
3. Os campos preenchíveis serão referentes as informações necessárias para se fazer a solicitação de uma reserva de sala. Você saberão quais campos serão necessários e seus tipos(texto, data, número etc) ao obter as informações necessárias a partir da reunião de requisitos com o time de DAF;
4. O botão de envio deverá disparar a rotina para envio das informações preenchidas no formulário para o canal privado no Slack no formato de uma mensagem;
5. O botão cancelar deverá fazer a aplicação retornar a tela inicial de seleção de serviço citada em **1**;
6. A formatação da mensagem postada no canal privado deverá seguir o seguinte esquema:

	![Imagem de referência para a mensagem de solicitação de reserva de sala postada no Slack](/img/ref/FormatacaoMensagemSolicitacaoReservaSala.png)
	**Figura 1**: Imagem de referência para a mensagem de solicitação de reserva de sala postada no Slack

### Gerenciais

1.  Manter o código gerado durante o projeto atualizado neste repositório;

2.  Utilizar a Wiki do repositório para documentar: a) Procedimentos necessários para se utilizar a aplicação pelo usuário; b) Detalhes técnicos de como a aplicação funciona, com trechos curtos e importantes do código com explicações da função dos mesmos;

	2.1.  Adicional: desenvolver diagramas de sequência para as explicações. [Link de referência](<https://mermaidjs.github.io/>);

3.  Utilizar o Trello para organização do time, podendo tomar como modelo a board [Projeto(Modelo) @ DEV](<https://trello.com/b/qsp7dfIi/projetomodelo-dev>) no time [Dev @ TCI](https://trello.com/dev48546281/home). O _board_ deve pertencer ao time e o [usuário da TITAN](https://trello.com/titancomputacaointeligente) deve ser incluso como administrador;

4.  Utilizar o canal **#trainee_dev** para as discussões de desenvolvimento do projeto e testes de envio de mensagens pela aplicação, posteriormente, será definido o canal definitivo para postagens da aplicação;

5.  Marcar e executar uma ou mais reuniões de requisitos com o time de DAF atual para saber quais informações deverão ser inclusas no formulário digital desenvolvido;

6. Definir um líder de projeto que será a pessoa cobrada dos prazos pela diretoria e a pessoa a definir e cobrar os prazos da equipe, assim como definir atribuições dos membros.



### Técnicos
1. Linguagem utilizada: Python3;
2. Webhook utilizada: Webhook do Slack;
3. Interface da aplicação: PyQt(Gráfica);
4. Caso tenha sido necessária a criação de mais _branches_ durante o desenvolvimento do projeto, ao seu final, com a aplicação devidamente funcional, fazer o merge das _branhces_ criadas para a _master_.
5. Como ferramenta para gerar o _standalone_ executável da aplicação sugerimos utilizarem o _Pyinstaller_ ou o fbs;


Prazos:
----------------------

### Dia 05/06 - Definição de líder do projeto
Informar via e-mail dev@titanci.com.br


### Dia 09/06 - Data limite para Reunião de Requisitos
Data limite para já haver ocorrido pelo  menos uma reunião para coleta de requisitos com o time de _trainees_ de DAF.

Informar via e-mail dev@titanci.com.br


### Dia 09/06 - Relatório do andamento do projeto
O relatório de andamento do projeto é um documento em texto simples, podendo ser formatado em Latex caso interesse, que informa o que foi pesquisado, o que foi implementado, as maiores dificuldades enfrentadas, as dificuldades previstas que ainda virão e uma estimativa do quanto falta em tempo para o término do projeto.

Informar via e-mail dev@titanci.com.br

### Dia 15/06 - Entrega do projeto

1. O projeto funcional. Pode ser apresentado através de um teste executado no próprio canal **#trainee_dev** para conferirmos se de fato a aplicação funciona.

2. O repositório com o código fonte atualizado;

3. A Wiki com a documentação adequada para podermos manter o conhecimento adiquirido durante a pesquisa e desenvolvimento para gerações futuras;

4. A _board_ do trello utilizada;

Acompanhamento:
----------------------

Estaremos disponíveis para acompanhamento e para tirar quaisquer dúvidas que possam surgir tanto presencialmente quanto virtualmente, precisando apenas nos contatarem chamando
@ Doug, @ Guilherme Edington ou @ diiaz no canal **#trainee_dev** no Slack.

Também não exitem em contatar os integrantes dos Grupos 1 e 2 para para tirar dúvidas de suas implementações anteriores.