# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'erro.ui'
#
# Created by: PyQt5 UI code generator 5.12.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Error(object):
    def setupUi(self, Error):
        Error.setObjectName("Error")
        Error.setFixedSize(228, 75)
        self.falha = QtWidgets.QLabel(Error)
        self.falha.setGeometry(QtCore.QRect(20, 20, 191, 31))
        self.falha.setObjectName("falha")

        self.retranslateUi(Error)
        QtCore.QMetaObject.connectSlotsByName(Error)

    def retranslateUi(self, Error):
        _translate = QtCore.QCoreApplication.translate
        Error.setWindowTitle(_translate("Error", "ERRO"))
        self.falha.setText(_translate("Error", "         Falha ao enviar a mensagem. \n"
"Verifique sua Conexão com a internet."))




if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Error = QtWidgets.QDialog()
    ui = Ui_Error()
    ui.setupUi(Error)
    Error.show()
    sys.exit(app.exec_())
